<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'iikosoftware' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ezHh9DQ6FrovJiFxqKc7Z7u9BKNlSlfjGBG9fIw9UsWXR791DFyNlM4jBCKGlwCykogenWshGD7eryodYrsNug==');
define('SECURE_AUTH_KEY',  'wXxDORKEgDUrK09avlSw2TdJqUdfaLwY5hHywkYwGcbr7HmUeMUjC4dL0y8I1B4rE9+3wmtj57Gi7DfpfnWGyQ==');
define('LOGGED_IN_KEY',    '7p2GIIJw//QPEa19bz/8l0O54iJrz6akrSqRLudbLAUQScjkYGPvnCPzzKnmrk5bYBYjv9KZUsnoqVbWurmOUQ==');
define('NONCE_KEY',        'kAMPIX+8t0HizmFACsWN9Drm5He310z2EYLt1OQPIc7hNb5QVwpRnaA2xTmghs7qWFCVDZC1dPX+H+1K5usZUg==');
define('AUTH_SALT',        'kh/YA5PmixXFIiMVh0aOAEkJwAiRsmrNAECkXhGNat3onzWxJwbdNUfaBgE5zaT+tcUhHxwSuhYFbyT/0zLBIg==');
define('SECURE_AUTH_SALT', 'mOKfxYWyoiWx9fKOC7IBuVxkgt3fpKLu/CECUwWPEnep3jbURCscu/8hODOFe2BvJAn1EH5xDzadbzYbv1gbhw==');
define('LOGGED_IN_SALT',   'iIKy2MHfeAowGvyolQpGtY2Kd++YC6H3cNCdCT20+nMQPHQnrQZXMvejjcNgzVhhRgpKmHHwThZJNsLjHBdzbg==');
define('NONCE_SALT',       'FwkoHfLtrgUg3gjqgUDYFL85GGeCqc8wWVjfJfM/zW81wAJ7nbzMZIQgpQCa/lqy3FMQrQV33XVOzVAUfmJmrg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors',0);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
