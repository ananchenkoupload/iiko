<?php
/**
 * Template Name: Landing
 */
get_header();
get_template_part('template-parts/section', 'tall-hero');
get_template_part('template-parts/section', 'media-long-content');
get_template_part('template-parts/section', 'cta-module');
get_template_part('template-parts/section', 'video-product');
get_template_part('template-parts/section', 'content-with-media');
get_template_part('template-parts/section', 'landing-trustpilot');
get_template_part('template-parts/section', 'accordion');
get_footer();
?>