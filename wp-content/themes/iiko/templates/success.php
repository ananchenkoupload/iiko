<?php
/**
 * Template Name: Success
 */
get_header();
  get_template_part('template-parts/section', 'hero');
  get_template_part('template-parts/section', 'timeline');
  get_template_part('template-parts/section', 'rich-content');
  get_template_part('template-parts/section', 'callout');
get_footer();
?>