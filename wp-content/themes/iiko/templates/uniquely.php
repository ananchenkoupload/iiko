<?php
/**
 * Template Name: Uniquely
 */
get_header();
  get_template_part('template-parts/section', 'introduction');
  get_template_part('template-parts/section', 'feature-grid');
  get_template_part('template-parts/section', 'video-carousel');
  get_template_part('template-parts/section', 'trustpilot');
get_footer();
?>