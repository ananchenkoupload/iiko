/**
 * Component: Tabs
 * ------------------------------------------------------------------------------
 * Tabs component with control and content.
 *
 * @namespace tabs
 */
import debounce from 'lodash.debounce';
import {on, extendDefaults} from '../helpers/utils';
import cssClasses from '../helpers/css-classes';

/**
 * Create a new tabs instance.
 * @param {string} selector - The tabs selector.
 */
export default (selector, config) => {

  /**
   * Default settings.
   */
  const defaults = {
    autoPlay: false,
    delay: 5000,
  };

  /**
   * Selectors.
   */
  const selectors = {
    control: '[js-tabs="control"]',
    content: '[js-tabs="content"]',
    stage: '[js-tabs="stage"]',
  };

  /**
   * Global instances.
   */
  const container = document.querySelector(selector);
  const settings = extendDefaults(defaults, config);
  let current = 2;

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    control: container.querySelectorAll(selectors.control),
    content: container.querySelectorAll(selectors.content),
    stage: container.querySelector(selectors.stage),
  };

  /**
   * Initiate component.
   */
  function init() {
    setListeners();
    setDefaults();
    setResizeEvents();

    if (settings.autoPlay) {
      startAutoPlay();
    }
  }
  
  /**
   * Set the resize events.
   */
  function setResizeEvents() {
    on('resize', debounce(handleResizeEvent, 250));
  }

  /**
   * Handles the resize event.
   */
  function handleResizeEvent() {
    const active = getActiveContent();
    setStageHeight(active);
  }

  /**
   * Set event listeners.
   */
  function setListeners() {
    [...nodeSelectors.control].forEach((element) => {
      on('click', element, (event) => handleControlEvent(event));
    });
  }

  /**
   * Start the auto play.
   */
  function startAutoPlay() {
    const maximum = String([...nodeSelectors.control].length);

    window.setTimeout(() => {
      const index = String(current);

      toggleItem(index);

      if (index === maximum) {
        current = 1;
        startAutoPlay();
        return;
      }

      current++;
      startAutoPlay();
    }, settings.delay);
  }

  /**
   * Handle the control click event.
   * @param {object} event - The event object.
   */
  function handleControlEvent(event) {
    const target = event.target;
    const item = target.dataset.item;

    toggleItem(item);
  }

  /**
   * Toggle an item.
   * @param {String} id - The item ID.
   */
  function toggleItem(id) {
    const content = container.querySelector(`${selectors.content}[data-item="${id}"]`);

    if (itemOpen(id)) {
      return;
    }

    closeAllItems();
    openItem(id);
    setStageHeight(content);
  }

  /**
   * Returns an item object.
   * @param {String} id - The item ID.
   */
  function getItem(id) {
    const item = {};

    [...nodeSelectors.control].forEach((element) => {
      if (element.dataset.item === id) {
        item.control = element;
      }
    });

    [...nodeSelectors.content].forEach((element) => {
      if (element.dataset.item === id) {
        item.content = element;
      }
    });

    return item;
  }

  /**
   * Returns if the item is open.
   * @param {String} id - The item ID.
   */
  function itemOpen(id) {
    return getItem(id).content.classList.contains(cssClasses.active);
  }

  /**
   * Close all items.
   */
  function closeAllItems() {
    [...nodeSelectors.control].forEach((element) => {
      closeItem(element.dataset.item);
    });
  }

  /**
   * Sets the stage height.
   * @param {HTMLElement} item - The active item element.
   */
  function setStageHeight(item) {
    nodeSelectors.stage.style.height = `${item.offsetHeight}px`;
  }

  /**
   * Set defaults.
   */
  function setDefaults() {
    const element = nodeSelectors.content[0];
    const item = element.dataset.item;

    closeAllItems();
    openItem(item);
    setStageHeight(element);
  }

  /**
   * Opens an item.
   * @param {String} id - The item ID.
   */
  function openItem(id) {
    const item = getItem(id);
    const control = item.control;
    const content = item.content;

    content.classList.add(cssClasses.active);
    control.classList.add(cssClasses.active);
    current = id;
  }

  /**
   * Closes an item.
   * @param {String} id - The item ID.
   */
  function closeItem(id) {
    const item = getItem(id);
    const control = item.control;
    const content = item.content;

    content.classList.remove(cssClasses.active);
    control.classList.remove(cssClasses.active);
  }

  /**
   * Returns the active content.
   */
  function getActiveContent() {
    return container.querySelector(`${selectors.content}.${cssClasses.active}`);
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
