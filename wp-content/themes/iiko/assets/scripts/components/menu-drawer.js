/**
 * Component: Menu Drawer
 * ------------------------------------------------------------------------------
 * Functions the menu drawer sub menu.
 *
 * @namespace menuDrawer
 */
import SiteNav from '../components/site-nav';

/**
 * Create a new menu drawer instance.
 */
export default () => {

  /**
   * Selectors.
   */
  const selectors = {
    container: '[js-menu-drawer="container"]',
    siteNav: '[js-menu-drawer="siteNav"]',
  };

  /**
   * Global instances.
   */
  const container = document.querySelector(selectors.container);

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    hasChildren: container.querySelectorAll(selectors.hasChildren),
  };

  /**
   * Initiate component.
   */
  function init() {
    registerSiteNav();
  }

  /**
   * Registers the site nav.
   */
  function registerSiteNav() {
    SiteNav(selectors.siteNav).init();
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
}
