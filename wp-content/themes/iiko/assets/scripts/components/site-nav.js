/**
 * Component: Site Nav
 * ------------------------------------------------------------------------------
 * A site nav component with dropdowns.
 *
 * @namespace siteNav
 */
import {on} from '../helpers/utils';
import cssClasses from '../helpers/css-classes';
import * as a11y from '../helpers/a11y';

/**
 * Create a new site nav instance.
 */
export default (selector) => {

  /**
   * Selectors.
   */
  const selectors = {
    dropdown: '[js-site-nav="dropdown"]',
    dropdownToggle: '[js-site-nav="dropdownToggle"]',
    navItem: '[js-site-nav="navItem"]',
  };

  /**
   * Global instances.
   */
  const container = document.querySelector(selector);

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    dropdown: container.querySelectorAll(selectors.dropdown),
    dropdownToggle: container.querySelectorAll(selectors.dropdownToggle),
  };

  /**
   * Initiate the component.
   */
  function init() {
    setDropdownEvents();
  }

  /**
   * Set dropdown events.
   */
  function setDropdownEvents() {
    [...nodeSelectors.dropdownToggle].forEach((element) => {
      on('click', element, (event) => handleDropdownToggle(event));
    });
  }

  /**
   * Handle the dropdown toggle event.
   * @param {Object} event - The event object.
   */
  function handleDropdownToggle(event) {
    const target = event.target;
    const dropdown = getClosestDropdown(target);

    event.preventDefault();
    toggleDropdown(dropdown);
  }

  /**
   * Returns the closest dropdown.
   * @param {HTMLElement} element - Target element.
   */
  function getClosestDropdown(element) {
    const parent = element.closest(selectors.navItem);
    const dropdown = parent.querySelector(selectors.dropdown);
    
    return dropdown;
  }

  /**
   * Toggle the dropdown element.
   * @param {HTMLElement} dropdown - The target element.
   */
  function toggleDropdown(dropdown) {
    return dropdownIsOpen(dropdown) ? closeDropdown(dropdown) : openDropdown(dropdown);
  }

  /**
   * Returns if the given dropdown is open.
   * @param {HTMLElement} dropdown - The dropdown element.
   */
  function dropdownIsOpen(dropdown) {
    return dropdown.classList.contains(cssClasses.open);
  }

  /**
   * Opens a given dropdown element.
   * @param {HTMLElement} dropdown - The target dropdown.
   */
  function openDropdown(dropdown) {
    const parent = dropdown.closest(selectors.navItem);
    const toggle = parent.querySelector(selectors.dropdownToggle);

    closeActiveDropdowns();

    dropdown.classList.add(cssClasses.open);
    a11y.setARIA(toggle, 'expanded', 'true');
  }

  /**
   * Closes a given dropdown element.
   * @param {HTMLElement} dropdown - The target dropdown.
   */
  function closeDropdown(dropdown) {
    const parent = dropdown.closest(selectors.navItem);
    const toggle = parent.querySelector(selectors.dropdownToggle);

    dropdown.classList.remove(cssClasses.open);
    a11y.setARIA(toggle, 'expanded', 'false');
  }

  /**
   * Close the currently active dropdowns.
   */
  function closeActiveDropdowns() {
    [...nodeSelectors.dropdown].forEach((element) => {
      if (dropdownIsOpen(element)) {
        closeDropdown(element);
      }
    });
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
}
