/**
 * Component: Site Header
 * ------------------------------------------------------------------------------
 * Functions the global site header.
 *
 * @namespace siteHeader
 */
import {extendDefaults, on} from '../helpers/utils';
import cssClasses from '../helpers/css-classes';
import SiteNav from '../components/site-nav';

/**
 * Create a new site header instance.
 */
export default (config) => {

  /**
   * Selectors.
   */
  const selectors = {
    callToAction: '[js-site-header="callToAction"]',
    container: '[js-site-header="container"]',
    siteNav: '[js-site-header="siteNav"]',
  };

  /**
   * Defaults.
   */
  const defaults = {
    detachOffset: 50,
    sticky: true,
  };

  /**
   * Settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    callToAction: document.querySelector(selectors.callToAction),
    container: document.querySelector(selectors.container),
    dropdown: document.querySelectorAll(selectors.dropdown),
    dropdownToggle: document.querySelectorAll(selectors.dropdownToggle),
  };

  /**
   * Initiate the component.
   */
  function init() {
    registerSiteNav();
    setClickEvents();

    if (settings.sticky) {
      setStickyHeader();
      setScrollEvents();
      setResizeEvents();

      if (!headerIsFixed()) {
        setBodyPadding();
      }
    }
  }

  /**
   * Returns if the header is fixed.
   */
  function headerIsFixed() {
    return nodeSelectors.container.classList.contains(cssClasses.fixed);
  }

  /**
   * Set scroll events.
   */
  function setScrollEvents() {
    on('scroll', () => handleScrollEvent());
  }

  /**
   * Set resize events.
   */
  function setResizeEvents() {
    on('resize', () => handleResizeEvent());
  }

  /**
   * Set click events.
   */
  function setClickEvents() {
    on('click', nodeSelectors.callToAction, () => {
      Theme.EventBus.emit('Toggle:MenuDrawer:close');
    });
  }

  /**
   * Handle the resize event.
   */
  function handleResizeEvent() {
    setStickyHeader();
  }

  /**
   * Handle the scroll event.
   */
  function handleScrollEvent() {
    if (window.scrollY <= settings.detachOffset) {
      nodeSelectors.container.classList.remove(cssClasses.detached);
      return;
    }

    nodeSelectors.container.classList.add(cssClasses.detached);
  }

  /**
   * Set the sticky header.
   */
  function setStickyHeader() {
    nodeSelectors.container.classList.add(cssClasses.sticky);
  }

  /**
   * Set the body padding.
   */
  function setBodyPadding() {
    document.body.style.paddingTop = `${getHeaderHeight()}px`;
  }

  /**
   * Returns the header height.
   */
  function getHeaderHeight() {
    return nodeSelectors.container.offsetHeight;
  }

  /**
   * Registers the site nav.
   */
  function registerSiteNav() {
    SiteNav(selectors.siteNav).init();
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
}
