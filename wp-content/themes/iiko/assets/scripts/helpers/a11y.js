/**
 * Helper: a11y
 * ------------------------------------------------------------------------------
 * Accessibility scripts for toggleable elements.
 *
 * @namespace a11y
 */
import * as focusTrap from 'focus-trap';
import {on, off} from './utils';

/**
 * Trap focus inside of a container.
 * @param {HTMLElement} container - The container element.
 */
export function createFocusTrap(container) {
  const options = {
    clickOutsideDeactivates: true,
  }

  return focusTrap(container, options);
}

/**
 * 
 * @param {HTMLElement} container - The container element.
 * @param {String} state - On or Off.
 */
export function escEvent(container, state, namespace) {
  if (state === 'on') {
    on('keydown', container, (event) => handleKeyDownEvent(event));
  } else if (state === 'off') {
    off('keydown', container, (event) => handleKeyDownEvent(event));
    return;
  }

  /**
   * Handles the keydown press event.
   * @param {Event} event - The event.
   */
  function handleKeyDownEvent(event) {
    if (!keyPressIsEsc(event)) {
      return;
    }

    Theme.EventBus.emit(`EscEvent:${namespace}:on`);
  }

  /**
   * Returns true if keypress is escape.
   * @param {Event} event - The event.
   */
  function keyPressIsEsc(event) {
    return event.keyCode === 27;
  }
}

/**
 * Set ARIA values.
 * @param {HTMLElement} elem - The element to set.
 * @param {String} state - The ARIA state to set, i.e. hidden.
 * @param {String} value - The value of the state.
 */
export function setARIA(elem, state, value) {
  if (elem === 'undefined') {
    return;
  }

  elem.setAttribute(`aria-${state}`, value);
}

/**
 * Remove from the a11y tree.
 * @param {HTMLElement} container - The container to remove.
 */
export function hideFromTree(container) {
  const elements = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
  const focusable = container.querySelectorAll(elements);

  [...focusable].forEach((element) => {
    element.setAttribute('tabindex', '-1');
  });

  setARIA(container, 'hidden', 'true');
}

/**
 * Add to the a11y tree.
 * @param {HTMLElement} container - The container to add.
 */
export function addToTree(container) {
  const elements = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
  const focusable = container.querySelectorAll(elements);

  [...focusable].forEach((element) => {
    element.setAttribute('tabindex', '0');
  });

  setARIA(container, 'hidden', 'false');
}