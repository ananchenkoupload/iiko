/**
 * Section: Video
 * ------------------------------------------------------------------------------
 * A video banner.
 *
 * @namespace video
 */
import {register} from '../helpers/sections';
import {on} from '../helpers/utils';
import cssClasses from '../helpers/css-classes';
import YouTubePlayer from 'youtube-player';

/**
 * Selectors.
 */
const selectors = {
  video: '[js-video="video"]',
  videoPlay: '[js-video="videoPlay"]',
};

/**
 * Register a new `video` section.
 */
register('video', {

  /**
   * Register node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      video: this.container.querySelector(selectors.video),
      videoPlay: this.container.querySelector(selectors.videoPlay),
    };
  },

  /**
   * Initiate the section.
   */
  init() {
    this.setNodeSelectors();
    this.registerYouTubePlayer();
    this.setNodeSelectors();
    this.setPlayerDefaults();
    this.setClickEvents();
  },

  /**
   * Set the click events.
   */
  setClickEvents() {
    on('click', this.nodeSelectors.videoPlay, () => this.handleVideoPlayClick());
  },

  /**
   * Handle the video play click.
   */
  handleVideoPlayClick() {
    const videoEmbed = this.container.querySelector(selectors.video);
      
    this.player.getPlayerState().then((response) => {
      if (response === 1) {
        this.player.pauseVideo();
        videoEmbed.classList.remove(cssClasses.active);
        this.nodeSelectors.videoPlay.classList.remove(cssClasses.hidden);

        return;
      }

      this.player.playVideo();
      videoEmbed.classList.add(cssClasses.active);
      this.nodeSelectors.videoPlay.classList.add(cssClasses.hidden);
    });
  },

  /**
   * Register the YouTube player.
   */
  registerYouTubePlayer() {
    this.player = YouTubePlayer(this.nodeSelectors.video, {
      height: '592px',
      playerVars: {
        rel: 0,
      },
      width: '100%',
    });
  },

  /**
   * Set the player defaults.
   */
  setPlayerDefaults() {
    this.player.cueVideoById(this.nodeSelectors.video.dataset.video);
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});