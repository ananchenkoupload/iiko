/**
 * Section: Example
 * ------------------------------------------------------------------------------
 * An example section.
 *
 * @namespace example
 */
import {register} from '../helpers/sections';
import {on} from '../helpers/utils';

/**
 * Selectors.
 */
const selectors = {
  button: '[js-example="button"]',
};

/**
 * Register a new `example` section.
 */
register('example', {

  /**
   * Register node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      button: this.container.querySelector(selectors.button),
    };
  },

  /**
   * Initiate the section.
   */
  init() {
    this.setNodeSelectors();
    this.setListeners();
  },

  /**
   * Set listeners.
   */
  setListeners() {
    on('click', this.nodeSelectors.button, () => {
      console.log('Click.');
    });
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});