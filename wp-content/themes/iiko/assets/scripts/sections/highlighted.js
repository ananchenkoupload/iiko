/**
 * Section: Highlighted
 * ------------------------------------------------------------------------------
 * Highlighted article posts.
 *
 * @namespace highlighted
 */
import Flickity from 'flickity';
import {register} from '../helpers/sections';
import {on} from '../helpers/utils';

/**
 * Selectors.
 */
const selectors = {
  item: '[js-highlighted="item"]',
  items: '[js-highlighted="items"]',
};

/**
 * Register a new `highlighted` section.
 */
register('highlighted', {

  /**
   * Register node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      items: this.container.querySelector(selectors.items),
      prevNextButtons: false,
    };
  },

  /**
   * Initiate the section.
   */
  init() {
    this.setNodeSelectors();
    this.setResizeEvents();
    this.handleResizeEvent();
  },

  /**
   * Set reszie events.
   */
  setResizeEvents() {
    on('resize', () => this.handleResizeEvent());
  },

  /**
   * Handle the resize events.
   */
  handleResizeEvent() {
    if (window.matchMedia('(max-width: 1024px)').matches) {
      this.registerCarousel();
      return;
    }

    this.destroyCarousel();
  },

  /**
   * Register the carousel.
   */
  registerCarousel() {
    this.carousel = new Flickity(selectors.items, {
      autoPlay: 4000,
      adaptiveHeight: true,
      cellSelector: selectors.item,
      pauseAutoPlayOnHover: false,
      prevNextButtons: false,
    });
  },

  /**
   * Destroy the carousel.
   */
  destroyCarousel() {
    if (this.carousel) {
      this.carousel.destroy();
    }
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});