/**
 * Section: Video Carousel
 * ------------------------------------------------------------------------------
 * Displays content with video cards in a carousel.
 *
 * @namespace videoCarousel
 */
import Flickity from 'flickity';
import YouTubePlayer from 'youtube-player';
import {register} from '../helpers/sections';

/**
 * Selectors.
 */
const selectors = {
  carousel: '[js-video-carousel="carousel"]',
  cell: '[js-video-carousel="cell"]',
  player: '[js-video-carousel="player"]',
};

/**
 * Instance globals.
 */
const videos = [];

/**
 * Register a new `video-carousel` section.
 */
register('video-carousel', {

  /**
   * Register node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      player: this.container.querySelectorAll(selectors.player),
    };
  },

  /**
   * Initiate the section.
   */
  init() {
    this.setNodeSelectors();
    this.registerYouTubePlayer();
    this.setNodeSelectors();
    this.registerCarousel();
    this.setVideoListeners();
    this.setPlayerDefaults();

    window.setTimeout(() => this.reloadCarousel(), 500);
  },

  /**
   * Register the YouTube player.
   */
  registerYouTubePlayer() {
    
    [...this.nodeSelectors.player].forEach((element) => {
      const video = {};

      video.player = YouTubePlayer(element, {
        height: '300px',
        playerVars: {
          rel: 0,
        },
        width: '100%',
      });

      video.element = element;
      videos.push(video);
    });
  },

  /**
   * Set video listeners.
   */
  setVideoListeners() {
    videos.forEach((video) => {
      video.player.addEventListener('onReady', 'reloadVideoCarousel');
    });
  },

  /**
   * Set the player defaults.
   */
  setPlayerDefaults() {
    videos.forEach((video) => {
      video.player.cueVideoById(video.element.dataset.video);
    });
  },

  /**
   * Register the carousel.
   */
  registerCarousel() {
    this.carousel = new Flickity(selectors.carousel, {
      cellAlign: 'left',
      cellSelector: selectors.cell,
      pageDots: true,
      prevNextButtons: false,
    });
  },

  /**
   * Reloads the carousel.
   */
  reloadCarousel() {
    this.carousel.reloadCells();
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});