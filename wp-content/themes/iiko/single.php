<?php
/**
 * Single post template.
 */

get_header(); ?>
  <?php
  if( have_posts() ):
    /**
     * Start the loop.
     */
    while( have_posts() ): the_post();
  ?>
      <div class="container">
        <div class="row">
          <div class="col xs12 l9">
            <article id="<?php the_ID(); ?>" <?php post_class(); ?> role="article">
              <?php if ( has_post_thumbnail() ): ?>
                <div
                  class="post__thumbnail lazyload"
                  data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
                >
                </div>
                <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="print-thumbnail">
              <?php endif; ?>

              <?php
              get_template_part('template-parts/post', 'content');

              /**
               * Post comments.
               */
              if ( comments_open() || get_comments_number() ):
                comments_template();
              endif;
              ?>
            </article>
          </div>
          <div class="col xs12 l3">
            <aside class="post__sidebar" role="complementary">
              <?php if ( have_rows('highlighted_posts') ): ?>
                <div class="post__highlighted highlighted">
                  <h3 class="highlighted__title"><?php _e('Highlighted', 'iiko'); ?></h3>

                  <svg viewBox="0 0 20 40" class="icon icon__highlighted" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                      <path id="a" d="M1235 41h20v40h-20z"/>
                    </defs>
                    <g transform="translate(-1235 -41)" fill="none" fill-rule="evenodd">
                      <image x="1235" y="41" width="20" height="40" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAoCAYAAAD+MdrbAAAAAXNSR0IArs4c6QAAAKZJREFUSA3t11EKgCAMBmCNLtStwgvVrTqSuXRgM11NoZcNJAX9tJ8ezB7GeDOwpoHWRSnYn6hmqBkKEtDPRhAaWaIZkkAEQ81QEBpZohmSQATD3zLcw2GhsTWzMyLksnlr1i+6HAincku6Q4a7JMJVtJXhDYOjJBjQ6uvXwAIDEIpDn8AqFsk2SkEW49AcfI01UfgLCG0LzeLEr09YmwxvejHcHNET/9cq8/erzfsAAAAASUVORK5CYII="/>
                      <use fill-opacity="0" fill="#000" xlink:href="#a"/>
                    </g>
                  </svg>

                  <?php
                  while ( have_rows('highlighted_posts') ): the_row();

                    $post = get_sub_field('post');
                    setup_postdata($post);
                  ?>
                    <div class="card card--snippet">
                      <?php if ( has_post_thumbnail() && get_row_index() === 1 ): ?>
                        <div
                          class="card__thumbnail lazyload"
                          data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
                        >
                        </div>
                      <?php endif; ?>

                      <a class="card__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      <p class="card__excerpt"><?php echo substr( get_the_excerpt(), 0, 94 ); ?>...</p>

                      <div class="card__meta">
                        <time datetime="<?php the_date( DATE_W3C ); ?>">
                          <?php echo get_the_date('l, jS F Y'); ?>
                        </time>
                      </div>
                    </div>
                  <?php
                    wp_reset_postdata();
                  endwhile;
                  ?>
                </div>
              <?php
              endif;

              if ( is_active_sidebar('post-sidebar') ):
                dynamic_sidebar('post-sidebar');
              endif;
              ?>
            </aside>
          </div>
        </div>
      </div>
  <?php
    endwhile;
  else:
  ?>
    <div class="container">
      <div class="row">
        <div class="col s12">
          <h1 class="no-content">
            <?php _e('Sorry, no posts matched your criteria.', 'iiko'); ?>
          </h1>
        </div>
      </div>
    </div>
  <?php endif; ?>
<?php get_footer(); ?>
