<?php
/**
 * Register menus.
 */
register_nav_menus(
  array(
    'main-menu' => 'Main Menu',
    'footer-menu-1' => 'Footer Menu 1',
    'footer-menu-2' => 'Footer Menu 2',
    'footer-menu-3' => 'Footer Menu 3',
    'footer-menu-4' => 'Footer Menu 4'
  )
);

/**
 * Add theme support.
 */
add_theme_support('custom-logo');
add_theme_support('title-tag');
add_theme_support('post-thumbnails');

/**
 * Register theme sidebars.
 */
function theme_register_sidebars() {
  register_sidebar( array(
    'name' => __('Archive Sidebar', 'iiko'),
    'id' => 'archive-sidebar',
    'description' => __('Widgets in this area will be shown on all archive pages', 'iiko'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget__title">',
    'after_title'   => '</h3>',
  ) );


  register_sidebar( array(
    'name' => __('Post Sidebar', 'iiko'),
    'id' => 'post-sidebar',
    'description' => __('Widgets in this area will be shown on all single posts.', 'iiko'),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget__title">',
    'after_title'   => '</h3>',
  ) );
}
add_action('widgets_init', 'theme_register_sidebars');

/**
 * WordPress head.
 */
function theme_meta_tags() {
  if ( is_archive() ):
    if ( get_the_archive_description() ):
      $description = get_the_archive_description();
    else:
      $description = get_bloginfo('description');
    endif;
  elseif ( is_single() || is_page() ):
    if ( is_home() || is_front_page() ):
      $description = get_bloginfo('description');
    else:
      if ( get_the_excerpt() ):
        $description = get_the_excerpt();
      else:
        $description = get_bloginfo('description');
      endif;
    endif;
  else:
    $description = get_bloginfo('description');
  endif;

  echo '<meta name="description" content="' . wp_strip_all_tags( $description ) . '">';
  echo '<meta name="format-detection" content="telephone=no">';
}
add_action('wp_head', 'theme_meta_tags');

/**
 * Enqueue layout scripts.
 */
function theme_enqueue_layout_scripts() {
  wp_enqueue_script( 'manifest', get_template_directory_uri() . '/assets/scripts/manifest.js', '', '', true );
  wp_enqueue_script( 'vendor', get_template_directory_uri() . '/assets/scripts/vendor.js', '', '', true );
  wp_enqueue_script( 'theme', get_template_directory_uri() . '/assets/scripts/theme.js', '', '', true );
}
add_action('wp_enqueue_scripts', 'theme_enqueue_layout_scripts');

/**
 * Enqueue styles.
 */
function theme_enqueue_stylesheets() {
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/assets/styles/theme.css' );
}
add_action('wp_enqueue_scripts', 'theme_enqueue_stylesheets');

/**
 * Returns the current menu items of a location.
 */
if ( !function_exists('get_menu_items') ):
  function get_menu_items( $location, $args = [] ) {
    $locations = get_nav_menu_locations();
    $object = wp_get_nav_menu_object( $locations[$location] );
    $menu_items = wp_get_nav_menu_items( $object->name, $args );

    return $menu_items;
  }
endif;

/**
 * Returns an svg.
 */
if ( !function_exists('include_svg') ):
  function include_svg( $name ) {
    echo file_get_contents( get_template_directory_uri() . '/assets/svgs/' . $name . '.svg' );
  }
endif;

/**
 * Add ACF Pro settings page.
 */
if ( function_exists('acf_add_options_page') ):
  acf_add_options_page( array(
    'page_title' => 'Theme Settings'
  ) );
endif;

/**
 * Strips out EXT characters.
 */
function stripExtCharacters($content) {
  $content = preg_replace('/\x03/', '', $content);
  return $content;
} 

add_filter('the_content', 'stripExtCharacters');
add_filter('the_title', 'stripExtCharacters');
