<?php if(get_field('show_hide') == 'yes'){ ?>
<section class="cta-module">
  <div class="container">
    <div class="cta-module__info">
    <?php if(get_field('cta_title')){ ?>
      <h2 class="cta-module__title"><?php echo get_field('cta_title'); ?></h2>
    <?php } ?>
    <?php if(get_field('cta_description')){ ?>
      <p><?php echo get_field('cta_description'); ?></p>
    <?php } ?>

    <?php
            if(get_field('cta_button')){
            $link = get_field('cta_button');
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>
    </div>
  </div>
</section>
<?php } ?>