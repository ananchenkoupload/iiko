<section
  class="pricing-plans"
  data-section-type="pricing-plans"
>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <?php if ( get_field('pricing_plans_title') ): ?>
          <h3 class="pricing-plans__title"><?php the_field('pricing_plans_title'); ?></h3>
        <?php endif; ?>
      </div>
    </div>

    <?php if ( have_rows('pricing_plans') ): ?>
      <div class="row">
        <div class="col xs12">
          <div class="pricing-plans__plans">
            <?php while ( have_rows('pricing_plans') ): the_row(); ?>
              <div class="pricing-plans__plan">
                <?php if ( get_sub_field('title') ): ?>
                  <h4 class="pricing-plans__plan-title"><?php the_sub_field('title'); ?></h4>
                <?php endif; ?>

                <?php if ( get_sub_field('subtitle') ): ?>
                  <p class="pricing-plans__plan-subtitle"><?php the_sub_field('subtitle'); ?></p>
                <?php endif; ?>

                <?php if ( get_sub_field('monthly_price') ): ?>
                  <p class="pricing-plans__plan-monthly">
                    <?php the_sub_field('monthly_price'); ?><span><?php _e('/mo', 'iiko'); ?></span>
                  </p>
                <?php endif; ?>

                <?php if ( get_sub_field('yearly_price') ): ?>
                  <p class="pricing-plans__plan-yearly">
                    <span><?php _e('or', 'iiko'); ?></span>
                    <?php the_sub_field('yearly_price'); ?><span><?php _e('/year', 'iiko'); ?></span>
                  </p>
                <?php endif; ?>

                <?php if ( get_sub_field('price_per') ): ?>
                  <p class="pricing-plans__plan-per"><?php the_sub_field('price_per'); ?></p>
                <?php endif; ?>

                <?php if ( get_sub_field('button_label') ): ?>
                  <a
                    class="button pricing-plans__plan-cta"
                    href="<?php the_sub_field('button_link'); ?>"
                  >
                    <?php the_sub_field('button_label'); ?>
                  </a>
                <?php endif; ?>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</section>