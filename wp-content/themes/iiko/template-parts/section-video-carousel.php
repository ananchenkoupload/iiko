<section
  class="video-carousel"
  data-section-type="video-carousel"
>
  <div class="container">
    <div class="row">
      <div class="col xs12 l4">
        <?php if ( get_field('video_carousel_title') ): ?>
          <h3 class="video-carousel__title"><?php the_field('video_carousel_title'); ?></h3>
        <?php endif; ?>

        <?php if ( get_field('video_carousel_content') ): ?>
          <p class="video-carousel__content"><?php the_field('video_carousel_content'); ?></p>
        <?php endif; ?>

        <?php if ( have_rows('video_carousel_cards') ): ?>
          <div class="video-carousel__carousel carousel" js-video-carousel="carousel">
            <?php while ( have_rows('video_carousel_cards') ): the_row(); ?>
              <div class="video-carousel__card" js-video-carousel="cell">
                <?php if ( get_sub_field('video_id') ): ?>
                  <div
                    class="video-carousel__card-player"
                    data-video="<?php the_sub_field('video_id'); ?>"
                    js-video-carousel="player"
                  >
                  </div>
                <?php endif; ?>

                <div class="video-carousel__card-body">
                  <?php if ( get_sub_field('title') ): ?>
                    <h4 class="video-carousel__card-title"><?php the_sub_field('title'); ?></h4>
                  <?php endif; ?>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>