<?php
/**
 * Single post content.
 */
?>
<div class="row">
  <div class="col xs12 l4">
    <header class="post__header">
      <h1 class="post__title"><?php the_title() ?></h1>

      <div class="post__meta">
        <div class="post__date">
          <time datetime="<?php the_date( DATE_W3C ); ?>">
            <?php echo get_the_date('l, jS F Y'); ?>
          </time>
        </div>

        <button class="post__download" onclick="window.print();">
          <svg viewBox="0 0 17 18" class="icon icon__download" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
              <path id="a" d="M289 222h17v18h-17z"/>
            </defs>
            <g transform="translate(-289 -222)" fill="none" fill-rule="evenodd">
              <image x="289" y="222" width="17" height="18" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAASCAYAAAC9+TVUAAAAAXNSR0IArs4c6QAAAMBJREFUOBHtk7sNwkAQRI+fhMjICOmAkDYIqYQiiOkDiqAYKkBkMA+402h9NiCnjDTaz8yuz7+U2rGSdDdSVzGsdl/NedBiXeSuJcX0KfkvaT6hcbOVFupNRaKDeinexIvYiomUq+jfR8zR8XViKzUOeo3+FXZy+WDO6f+Eg9x5mEjdipMUuA6OkeqjyAIitQN/ni1X27jjnc8UuQViBP7nSWuv2M28ib03arkvibdT83uv+AfqcqRe4C8+99qg+Qdssy+5uKWWNAAAAABJRU5ErkJggg=="/>
              <use fill-opacity="0" fill="#000" xlink:href="#a"/>
            </g>
          </svg>
        </button>
      </div>
    </header>
  </div>

  <div class="col xs12 l8">
    <div class="post__content">
      <?php the_content(); ?>
    </div>

    <?php if ( have_rows('related_posts') ): ?>
      <div class="row">
        <div class="col xs12">
          <h3 class="post__related-title"><?php _e('Read more stories', 'iiko'); ?></h3>
        </div>
      </div>

      <div class="row">
        <?php
        while ( have_rows('related_posts') ): the_row();
          $post = get_sub_field('post');
          setup_postdata($post);
        ?>
          <div class="col xs12 s6">
            <div class="card">
              <?php if ( has_post_thumbnail() ): ?>
                <a
                  class="card__thumbnail lazyload"
                  data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
                >
                </a>
              <?php endif; ?>

              <div class="card__body">
                <a class="card__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                <p class="card__excerpt"><?php echo substr( get_the_excerpt(), 0, 94 ); ?>...</p>

                <div class="card__meta">
                  <time datetime="<?php the_date( DATE_W3C ); ?>">
                    <?php echo get_the_date('l, jS F Y'); ?>
                  </time>
                </div>
              </div>
            </div>
          </div>
        <?php
          wp_reset_postdata();
        endwhile;
        ?>
      </div>
    <?php endif; ?>
  </div>
</div>

<?php
/**
 * Pagination for the content.
 */
wp_link_pages( array(
  'before' => '<ul class="pagination">',
  'after' => '</ul>',
  'link_before' => '<li class="pagination__part">',
  'link_after' => '</li>'
) );
?>
