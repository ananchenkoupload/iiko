<div
  id="MenuDrawer"
  class="menu-drawer"
  aria-hidden="true"
  js-menu-drawer="container"
  tabindex="-1"
>
  <div class="menu-drawer__container">
    <div class="menu-drawer__header">
      <button class="menu-drawer__close" js-toggle="MenuDrawer">
        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon__close" viewBox="0 0 36 36">
          <g stroke="#000" stroke-width="3" fill="none" fill-rule="evenodd" stroke-linecap="square">
            <path d="M7.5 7.5l21 21M28.5 7.5l-21 21"/>
          </g>
        </svg>

        <span class="visually-hidden">
          <?php _e('Close menu drawer', 'iiko'); ?>
        </span>
      </button>
    </div>
    <div class="menu-drawer__body">
      <nav class="menu-drawer__nav col s12" js-menu-drawer="siteNav" role="navigation">
        <?php get_template_part('template-parts/site', 'nav'); ?>
      </nav>
    </div>

    <div class="menu-drawer__footer">
      <?php if ( get_field('header_call_to_action', 'option') ): ?>
        <a
          class="menu-drawer__cta button button--outlined"
          href="<?php the_field('header_call_to_action_link', 'option'); ?>"
          js-site-header="callToAction"
        >
          <?php the_field('header_call_to_action', 'option'); ?>
        </a>
      <?php endif; ?>
    </div>
  </div>
</div>
