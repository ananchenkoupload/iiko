<section class="product-hero">
  <div class="product-hero__pattern"></div>
  <div class="container">
    <div class="row">
      <div class="col xs12 l6">
        <div class="product-hero__image">
        <?php if(get_field('product_hero_image')){ ?>
          <img src="<?php echo get_field('product_hero_image'); ?>">
        <?php } ?>
        </div>
      </div>
      <div class="col xs12 l6">
        <div class="product-hero__content">
        <?php if(get_field('product_hero_title')){ ?>
          <h2 class="product-hero__title"><?php echo get_field('product_hero_title'); ?></h2>
        <?php } ?>
        <?php if(get_field('product_hero_subtitle')){ ?>
          <h3 class="product-hero__sub-title"><?php echo get_field('product_hero_subtitle'); ?></h3>
        <?php } ?>
        <?php if(get_field('product_hero_text')){ ?>
          <p class="product-hero__text"><?php echo get_field('product_hero_text'); ?></p>
        <?php } ?>
          <?php
             if(get_field('product_hero_list')){
               echo get_field('product_hero_list');
             }
           ?>
          <div class="product-hero__pricing-cart">
            
          <?php
            if(get_field('product_hero_link')){
            $link = get_field('product_hero_link');
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>

            <div class="product-hero__pricing-box">
            <?php if(get_field('product_hero_price')){ ?>
              <h4 class="product-hero__price"><?php echo get_field('product_hero_price'); ?></h4>
            <?php } ?>
            <?php if(get_field('product_hero_currency')){ ?>
              <span><?php echo get_field('product_hero_currency'); ?>
            <?php } ?>

              <br/>
              <?php if(get_field('product_hero_value')){ ?>
              <?php echo get_field('product_hero_value'); ?></span>
              <?php } ?>
            </div>
          </div>
          <?php if(get_field('product_hero_status')){ ?>
          <h5 class="product-hero__status"><?php echo get_field('product_hero_status'); ?></h5>
          <?php } ?>
        </div>
      </div>
    </div>
  </div> 
</section>