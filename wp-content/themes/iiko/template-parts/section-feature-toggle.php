<section
  class="feature-toggle tabs"
  data-section-type="feature-toggle"
>
  <div class="container">
    <div class="row">
      <div class="col xs12 l6">
        <?php if ( get_field('feature_toggle_title') ): ?>
          <h3 class="feature-toggle__title"><?php the_field('feature_toggle_title'); ?></h3>
        <?php endif; ?>

        <?php if ( get_field('feature_toggle_content') ): ?>
          <p class="feature-toggle__content"><?php the_field('feature_toggle_content'); ?></p>
        <?php endif; ?>

        <?php if ( have_rows('feature_toggle_items') ): ?>
          <div class="feature-toggle__control-group row">
            <?php while ( have_rows('feature_toggle_items') ): the_row(); ?>
              <div class="col xs6">
                <button
                  class="feature-toggle__control"
                  data-item="<?php the_row_index(); ?>"
                  js-tabs="control"
                >
                  <?php the_sub_field('title'); ?>
                </button>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>

      <div class="col xs12 l6">
        <?php if ( have_rows('feature_toggle_items') ): ?>
          <div class="feature-toggle__content-group tabs__stage" js-tabs="stage">
            <?php while ( have_rows('feature_toggle_items') ): the_row(); ?>
              <div
                class="tabs__content feature-toggle__content-block"
                data-item="<?php the_row_index(); ?>"
                js-tabs="content"
              >
                <?php if ( get_sub_field('title') ): ?>
                  <h4 class="feature-toggle__item-title"><?php the_sub_field('title'); ?></h4>
                <?php endif; ?>

                <?php if ( get_sub_field('image') ): ?>
                  <img
                    class="feature-toggle__item-image"
                    src="<?php the_sub_field('image'); ?>"
                  >
                <?php endif; ?>

                <?php if ( get_sub_field('content') ): ?>
                  <p class="feature-toggle__item-content"><?php the_sub_field('content'); ?></p>
                <?php endif; ?>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>