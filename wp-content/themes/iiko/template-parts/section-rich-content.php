<section
  class="rich-content"
  data-section-type="rich-content"
>
  
        <?php
        if ( have_rows('rich_content_sections') ):
          while ( have_rows('rich_content_sections') ): the_row();
        ?>
            <div class="rich-content__section">
              <?php if ( get_sub_field('title_1') || get_sub_field('title_2') ): ?>
                <div class="container">
                  <div class="row">
                    <div class="col xs12">
                      <h3 class="rich-content__title">
                        <span><?php the_sub_field('title_1'); ?></span>
                        <span><?php the_sub_field('title_2'); ?></span>
                      </h3>
                    </div>
                  </div>
                </div>
              <?php endif; ?>

              <?php if ( get_sub_field('image') ): ?>
                <div class="rich-content__image lazyload" data-bgset="<?php the_sub_field('image'); ?>"></div>
              <?php endif; ?>

              <?php if ( get_sub_field('content') ): ?>
                <div class="rich-content__content"><?php the_sub_field('content'); ?></div>
              <?php endif; ?>

              <?php if ( get_sub_field('questions') ): ?>
                <div class="rich-content__questions">
                  <h4 class="rich-content__questions-title"><?php _e('Ask yourself...', 'iiko'); ?></h4>
                  <p class="rich-content__questions-content"><?php the_sub_field('questions'); ?></p>
                </div>
              <?php endif; ?>
            </div>
        <?php
          endwhile;
        endif;
        ?>
      </div>
    </div>
  </div>
</section>