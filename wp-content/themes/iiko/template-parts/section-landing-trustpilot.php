

        <section class="trustpilot trustpilot--<?php echo get_field('trustpilot_image_position'); ?> trustpilot--without-pattern" data-section-type="trustpilot">
        <div class="trustpilot__body">
            <div class="container">
            <div class="row align-center">
                <div class="col xs12 l5 trustpilot-info">
                <?php if(get_field('trustpilot_header_image')){ ?>
                <img class="trustpilot__header-image" alt="Trusted. Proven. Experienced." src="<?php echo get_field('trustpilot_header_image'); ?>">
                <?php } ?>
                <?php if(get_field('trustpilot_title')){ ?>
                <h3 class="trustpilot__title"><?php echo get_field('trustpilot_title'); ?></h3>
                <?php } ?>
                <?php if(get_field('trustpilot_content')){ ?>
                <p class="trustpilot__content"><?php echo get_field('trustpilot_content'); ?></p>
                <?php } ?>
                <div class="trustpilot__review">
                    <?php if(get_field('trustpilot_rating')){ ?>
                    <img class="trustpilot__logo" alt="Trustpilot client logo" src="<?php echo get_field('trustpilot_rating'); ?>">
                    <?php } ?>
                    <?php  ?>
                    <?php if(get_field('trustpilot_review')){ ?>
                    <h4 class="trustpilot__subtitle"><?php echo get_field('trustpilot_review'); ?></h4>
                    <?php } ?>
                </div>
                </div>
                <div class="col xs12 l7 trustpilot-img">
                <?php if(get_field('trustpilot_main_image')){ ?>
                <img class="trustpilot__main-image" alt="Trusted. Proven. Experienced." src="<?php echo get_field('trustpilot_main_image'); ?>">
                <?php } ?>
                </div>
            </div>
            </div>
        </div>
        </section>