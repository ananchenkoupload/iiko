<section
  id="NewsletterCallout"
  class="newsletter-callout"
  data-section-type="newsletter-callout"
>
  <div class="newsletter-callout__wrapper">
    <div class="container">
      <div class="newsletter-callout__container">
        <div class="newsletter-callout__header">
          <?php if ( get_field('newsletter_callout_title', 'option') ): ?>
            <h3 class="newsletter-callout__title">
              <?php the_field('newsletter_callout_title', 'option'); ?>
            </h3>
          <?php endif; ?>

          <?php if ( get_field('newsletter_callout_subtitle', 'option') ): ?>
            <p class="newsletter-callout__subtitle">
              <?php the_field('newsletter_callout_subtitle', 'option'); ?>
            </p>
          <?php endif; ?>
        </div>

        <div class="newsletter-callout__form">
          <!--[if lte IE 8]>
          <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
          <![endif]-->
          <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
          <script>
          hbspt.forms.create({
          portalId: "6316159",
          formId: "bd92596f-165b-4013-946f-796e45be2e77"
          });
          </script>
        </div>
        <!-- <form class="newsletter-callout__form">
          <div class="form__field">
            <input
              id="Name"
              aria-label="<?php _e('Full Name'); ?>"
              name="NAME"
              placeholder="<?php _e('Full Name'); ?>"
              type="text"
            >
          </div>

          <div class="form__field">
            <input
              id="Email"
              aria-label="<?php _e('Email'); ?>"
              name="EMAIL"
              placeholder="<?php _e('Email'); ?>"
              type="email"
            >
          </div>

          <div class="form__field">
            <input
              id="Phone"
              aria-label="<?php _e('Phone Number'); ?>"
              name="PHONE"
              placeholder="<?php _e('Phone Number'); ?>"
              type="number"
            >
          </div>

          <div class="form__field">
            <button class="newsletter-callout__submit button button--light" type="submit">
              <?php the_field('newsletter_callout_submit', 'option'); ?>
            </button>
          </div>
        </form> -->
      </div>
    </div>
  </div>
</section>
