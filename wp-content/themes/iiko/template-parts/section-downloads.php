<section
  class="downloads"
  data-section-type="downloads"
>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <?php if ( get_field('downloads_title') ): ?>
          <h3 class="downloads__title"><?php the_field('downloads_title'); ?></h3>
        <?php endif; ?>
      </div>

      <?php $counter = 1; ?>
      <?php if ( have_rows('downloads_items') ): ?>
        <div class="col xs12">
          <div class="row">
            <?php while ( have_rows('downloads_items') ): the_row(); ?>
              <div class="col xs12 m6">
                <div class="downloads__item">
                  <div class="downloads__item-body">
                    <?php if ( get_sub_field('subtitle') ): ?>
                      <p class="downloads__item-subtitle"><?php the_sub_field('subtitle'); ?></p>
                    <?php endif; ?>

                    <?php if ( get_sub_field('title') ): ?>
                      <h4 class="downloads__item-title"><?php the_sub_field('title'); ?></h4>
                    <?php endif; ?>

                    <?php if ( get_sub_field('content') ): ?>
                      <p class="downloads__item-content"><?php the_sub_field('content'); ?></p>
                    <?php endif; ?>
                  </div>

                  <div class="downloads__item-button">
                    <!-- <a download href="<?php the_sub_field('download'); ?>"> -->
                    <a js-modal="open" href="#bookmodal<?php echo $counter; $counter++; ?>">
                      <img
                        class="downloads__item-icon"
                        alt="<?php _e('Download ', 'iiko') . the_sub_field('title'); ?>"
                        src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-download.png"
                      >
                    </a>
                  </div>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php
  if (isset($_GET['growthguide'])) {
    $classes = 'is-active';
  } else {
    $classes = '';
  }

  if (isset($_GET['inventorycontrol'])) {
    $classes2 = 'is-active';
  } else {
    $classes2 = '';
  }
?>

<div class="ll-downpopup <?php echo $classes; ?>" id="bookmodal1">
  <div class="ll-downpopup__container">
    <h2>Please provide your details</h2>
    <section>Your free e-book is just one click away!</section>

    <div class="ll-downpopup__form">
      <!--[if lte IE 8]>
      <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
      <![endif]-->
      <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
      <script>
      hbspt.forms.create({
      portalId: "6316159",
      formId: "f5f0e912-df5a-4c61-9495-768485db45d1"
      });
      </script>
    </div>
  </div>

  <a href="#" class="ll-downpopup__close" js-modal="close"></a>
</div>

<div class="ll-downpopup <?php echo $classes2; ?>" id="bookmodal2">
  <div class="ll-downpopup__container">
    <h2>Please provide your details</h2>
    <section>Your free e-book is just one click away!</section>

    <div class="ll-downpopup__form">
      <!--[if lte IE 8]>
      <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
      <![endif]-->
      <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
      <script>
      hbspt.forms.create({
      portalId: "6316159",
      formId: "d0fe9198-70ec-439d-9292-874de286d944"
      });
      </script>
    </div>
  </div>

  <a href="#" class="ll-downpopup__close" js-modal="close"></a>
</div>
