<section
  class="feature-grid"
  data-section-type="feature-grid"
>
  <div class="container">
    <div class="row">
      <div class="col xs12 l6">
        <?php if ( !empty( get_field('feature_grid_header_image') ) ): ?>
          <img
            class="feature-grid__header-image lazyload"
            alt="<?php echo get_field('feature_grid_header_image')['alt'];  ?>"
            data-src="<?php echo get_field('feature_grid_header_image')['url']; ?>"
          >
        <?php endif; ?>
      </div>

      <div class="col xs12 l6">
        <?php if ( get_field('feature_grid_content') ): ?>
          <p class="feature-grid__content"><?php the_field('feature_grid_content'); ?></p>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <?php if ( have_rows('feature_grid_items') ): ?>
    <div class="container">
      <div class="row">
        <div class="col xs12">
          <div class="feature-grid__items carousel" js-feature-grid="items">
            <?php
            $item_loop = 1;

            while ( have_rows('feature_grid_items') ): the_row();
              if ( $item_loop === 1 ):
                echo '<div class="feature-grid__cell" js-feature-grid="cell">';
                echo '<div class="feature-grid__column is-firstcolumn">';
              elseif ( $item_loop === 4 ):
                echo '</div>';
                echo '<div class="feature-grid__column hide-mobile">';
              endif;
            ?>
              <div class="feature-grid__card-container <?php if ($item_loop == 5) { echo 'is-alt'; } ?>">
                <div class="feature-grid__card">
                  <a class="feature-grid__card-link" href="<?php the_sub_field('link'); ?>">
                    <?php if ( get_sub_field('image') ): ?>
                      <div
                        class="feature-grid__card-thumbnail lazyload"
                        data-bgset="<?php the_sub_field('image'); ?>"
                      >
                      </div>
                    <?php endif; ?>

                    <div class="feature-grid__card-body">
                      <?php if ( get_sub_field('title') ): ?>
                        <h4 class="feature-grid__card-title"><?php the_sub_field('title'); ?></h4>
                      <?php endif; ?>
                    </div>
                  </a>
                </div>
              </div>
            <?php
              if ( $item_loop === 4 ):
                echo '</div>';
                echo '</div>';
                $item_loop++;
            ?>
            <div class="feature-grid__cell" js-feature-grid="cell">
              <div class="feature-grid__column">
                <div class="feature-grid__card-container hide-desktop">
                  <div class="feature-grid__card">
                    <a class="feature-grid__card-link" href="<?php the_sub_field('link'); ?>">
                      <?php if ( get_sub_field('image') ): ?>
                        <div
                          class="feature-grid__card-thumbnail lazyload"
                          data-bgset="<?php the_sub_field('image'); ?>"
                        >
                        </div>
                      <?php endif; ?>

                      <div class="feature-grid__card-body">
                        <?php if ( get_sub_field('title') ): ?>
                          <h4 class="feature-grid__card-title"><?php the_sub_field('title'); ?></h4>
                        <?php endif; ?>
                      </div>
                    </a>
                  </div>
                </div>
            <?php
              else:
                $item_loop++;
              endif;
            endwhile;
            ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
</section>
