<section
  class="image-with-text"
  data-section-type="image-with-text"
>
  <?php if ( have_rows('image_with_text_sections') ): ?>
    <div class="container">
      <div class="row">
        <div class="col xs12">
          <?php while ( have_rows('image_with_text_sections') ): the_row(); ?>
            <div class="image-with-text__section">
              <div class="image-with-text__main">
                <?php if ( get_sub_field('title') ): ?>
                  <h3 class="image-with-text__title"><?php the_sub_field('title'); ?></h3>
                <?php endif; ?>

                <?php if ( get_sub_field('content') ): ?>
                  <p class="image-with-text__content"><?php the_sub_field('content'); ?></p>
                <?php endif; ?>
              </div>

              <div
                class="image-with-text__image-container lazyload"
                data-bgset="<?php the_sub_field('image'); ?>"
              >
              </div>

              <?php if ( get_sub_field('additional') ): ?>
                <div class="image-with-text__additional"><?php the_sub_field('additional'); ?></div>
              <?php endif; ?>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
</section>