<section
  class="feature-tabs tabs"
  data-section-type="feature-tabs"
>
  <div class="container">
    <div class="row">
      <div class="col xs12 l8 push-l2">
        <?php if ( get_field('feature_tabs_title') ): ?>
          <h3 class="feature-tabs__title"><?php the_field('feature_tabs_title'); ?></h3>
        <?php endif; ?>

        <?php if ( get_field('feature_tabs_content') ): ?>
          <p class="feature-tabs__content"><?php the_field('feature_tabs_content'); ?></p>
        <?php endif; ?>
      </div>

      <div class="col xs12">
        <?php if ( have_rows('feature_tabs') ): ?>
          <div class="feature-tabs__control-group">
            <?php while ( have_rows('feature_tabs') ): the_row(); ?>
              <button
                class="feature-tabs__control"
                data-item="<?php the_row_index(); ?>"
                js-tabs="control"
              >
                <?php the_sub_field('title'); ?>
              </button>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div class="feature-tabs__content-container container">
    <div class="row">
      <div class="col xs12">
        <?php if ( have_rows('feature_tabs') ): ?>
          <div class="feature-tabs__content-group tabs__stage" js-tabs="stage">
            <?php while ( have_rows('feature_tabs') ): the_row(); ?>
              <div
                class="tabs__content feature-tabs__content-block"
                data-item="<?php the_row_index(); ?>"
                js-tabs="content"
              >
                <div class="row">
                  <div class="col xs12 l6">
                    <div class="feature-tabs__item-header">
                      <?php if ( get_sub_field('heading_1') ): ?>
                        <h4 class="feature-tabs__item-heading"><?php the_sub_field('heading_1'); ?></h4>
                      <?php endif; ?>

                      <?php if ( get_sub_field('heading_2') ): ?>
                        <h4 class="feature-tabs__item-heading"><?php the_sub_field('heading_2'); ?></h4>
                      <?php endif; ?>

                      <?php if ( get_sub_field('heading_3') ): ?>
                        <h4 class="feature-tabs__item-heading"><?php the_sub_field('heading_3'); ?></h4>
                      <?php endif; ?>
                    </div>

                    <?php if ( get_sub_field('intro') ): ?>
                      <p class="feature-tabs__item-intro"><?php the_sub_field('intro'); ?></p>
                    <?php endif; ?>

                    <?php if ( get_sub_field('content') ): ?>
                      <p class="feature-tabs__item-content"><?php the_sub_field('content'); ?></p>
                    <?php endif; ?>
                  </div>

                  <div class="col xs12 l6">
                    <?php if ( get_sub_field('image') ): ?>
                      <img
                        class="feature-tabs__item-image"
                        alt="<?php the_sub_field('title'); ?>"
                        src="<?php the_sub_field('image'); ?>"
                      >
                    <?php endif; ?>

                    <?php if ( have_rows('features') ): ?>
                      <ul class="feature-tabs__item-features">
                        <?php while ( have_rows('features') ): the_row(); ?>
                        <?php if ( get_sub_field('text') ): ?>
                        <li class="feature-tabs__item-feature"><?php the_sub_field('text'); ?></li>
                        <?php endif; ?>
                        <?php endwhile; ?>
                      </ul>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>