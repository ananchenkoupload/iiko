<?php
/**
 * Single page content.
 */
?>
<article id="<?php the_ID(); ?>" <?php post_class(); ?> role="article">
  <div class="container">
    <div class="row">
      <div class="col xs12 l8 push-l2">
        <header class="page__header">
          <?php the_title('<h1 class="page__title">', '</h1>'); ?>
        </header>

        <div class="page__content">
          <?php the_content(); ?>
        </div>
        
        <?php
        /**
         * Pagination for the content.
         */
        wp_link_pages( array(
          'before' => '<nav class="pagination">',
          'after' => '</nav>',
          'link_before' => '<span class="page-numbers">',
          'link_after' => '</span>'
        ) );
        ?>
      </div>
    </div>
  </div>
</article>