<section class="center-trustpilot">
  <div class="container">
    <div class="center-trustpilot__header">
      <?php if(get_field('tpc_header_image')){ ?>
      <img class="center-trustpilot__header-image" alt="Trusted. Proven. Experienced." src="<?php echo get_field('tpc_header_image'); ?>">
      <?php } ?>
      <?php if(get_field('tpc_title')){ ?>
      <h3 class="center-trustpilot__title"><?php echo get_field('tpc_title'); ?></h3>
      <?php } ?>
    </div>  
    <div class="row">
    <?php if( have_rows('trust_pilot_cards') ){ ?>
        <?php while ( have_rows('trust_pilot_cards') ) : the_row(); ?>
            <div class="col xs12 m6">
                <div class="center-trustpilot-column">
                <?php if(get_sub_field('tpc_card_title')){ ?>
                <h5 class="center-trustpilot-column__title"><?php echo the_sub_field('tpc_card_title'); ?></h5>
                <?php } ?>
                <?php if(get_sub_field('tpc_card_sub_title')){ ?>
                <h6 class="center-trustpilot-column__sub-title"><?php echo the_sub_field('tpc_card_sub_title'); ?></h6>
                <?php } ?>
                <?php if(get_sub_field('tpc_card_content')){ ?>
                <p><?php echo the_sub_field('tpc_card_content'); ?></p>
                <?php } ?>
                <?php if(get_sub_field('tpc_card_image')){ ?>
                <img src="<?php echo the_sub_field('tpc_card_image'); ?>">
                <?php } ?>
                </div>
            </div>
    <?php endwhile; 
     } ?>
    </div>
  </div>  
</section>