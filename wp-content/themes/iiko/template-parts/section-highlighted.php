<?php if ( have_rows('highlighted_posts') ): ?>
  <div class="highlighted">
    <h3 class="highlighted__title"><?php _e('Highlighted', 'iiko'); ?></h3>

    <svg viewBox="0 0 20 40" class="icon icon__highlighted" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <defs>
        <path id="a" d="M1235 41h20v40h-20z"/>
      </defs>
      <g transform="translate(-1235 -41)" fill="none" fill-rule="evenodd">
        <image x="1235" y="41" width="20" height="40" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAoCAYAAAD+MdrbAAAAAXNSR0IArs4c6QAAAKZJREFUSA3t11EKgCAMBmCNLtStwgvVrTqSuXRgM11NoZcNJAX9tJ8ezB7GeDOwpoHWRSnYn6hmqBkKEtDPRhAaWaIZkkAEQ81QEBpZohmSQATD3zLcw2GhsTWzMyLksnlr1i+6HAincku6Q4a7JMJVtJXhDYOjJBjQ6uvXwAIDEIpDn8AqFsk2SkEW49AcfI01UfgLCG0LzeLEr09YmwxvejHcHNET/9cq8/erzfsAAAAASUVORK5CYII="/>
        <use fill-opacity="0" fill="#000" xlink:href="#a"/>
      </g>
    </svg>

    <?php
    while ( have_rows('highlighted_posts') ): the_row();

      $post = get_sub_field('post');
      setup_postdata($post);
    ?>
      <div class="card card--snippet">
        <?php if ( has_post_thumbnail() && get_row_index() === 1 ): ?>
          <div
            class="card__thumbnail lazyload"
            data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
          >
          </div>
        <?php endif; ?>

        <a class="card__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        <p class="card__excerpt"><?php echo substr( get_the_excerpt(), 0, 94 ); ?>...</p>

        <div class="card__meta">
          <time datetime="<?php the_date( DATE_W3C ); ?>">
            <?php echo get_the_date('l, jS F Y'); ?>
          </time>
        </div>
      </div>
    <?php
      wp_reset_postdata();
    endwhile;
    ?>
  </div>
<?php endif; ?>
