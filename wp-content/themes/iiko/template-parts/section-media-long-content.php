<?php if(get_field('show_hide') == 'yes'){ ?>
<section class="media-long-content">
  <div class="container">
    <div class="row">
      <div class="col xs12 l4">
        <div class="media-long-content__media">
          <?php if(get_field('media_image')){ ?>
            <img src="<?php echo get_field('media_image'); ?>" class="media-long-content__desk-img">
          <?php } ?>
          <?php if(get_field('media_image_mobile')){ ?>
             <img src="<?php echo get_field('media_image_mobile'); ?>" class="media-long-content__mobile-img">
          <?php } ?>
        </div>
      </div>
      <div class="col xs12 l8">
        <div class="media-long-content__content">
        <?php if(get_field('media_title')){ ?>
          <h2 class="media-long-content__title title-line-pattern"><?php echo get_field('media_title'); ?></h2>
        <?php } ?>
        <?php if(get_field('media_description')){ ?>
          <p class="media-long-content__text"><?php echo get_field('media_description'); ?></p>
        <?php } ?>
        <div class="media-long-content__info">
        <div class="media-long-content__column">
        <?php if( have_rows('media_long_content') ){ ?>
        <?php $i=1; ?>
            <?php while ( have_rows('media_long_content') ) : the_row(); ?>
                    <?php if(get_sub_field('media_body_title')){ ?>
                    <strong><?php echo the_sub_field('media_body_title'); ?></strong>
                    <?php } ?>
                    <?php if(get_sub_field('media_body_description')){ ?>
                    <p><?php echo the_sub_field('media_body_description'); ?></p>
                    <?php } ?>
                <?php if($i%3 == 0){ ?>
                  </div>
                  <div class="media-long-content__column">
                <?php } ?>
                <?php $i++; ?>
            <?php endwhile; ?>
        <?php } ?>
        </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>