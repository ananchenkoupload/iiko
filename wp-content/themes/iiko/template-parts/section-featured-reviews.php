<section
  class="featured-reviews"
  data-section-type="featured-reviews"
>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <?php if ( !empty( get_field('featured_reviews_header_image') ) ): ?>
          <img
            class="featured-reviews__header-image"
            alt="<?php echo get_field('featured_reviews_header_image')['alt']; ?>"
            src="<?php echo get_field('featured_reviews_header_image')['url']; ?>"
          >
        <?php endif; ?>

        <?php if ( get_field('featured_reviews_title') ): ?>
          <h3 class="featured-reviews__title"><?php the_field('featured_reviews_title'); ?></h3>
        <?php endif; ?>

        <?php if ( have_rows('featured_reviews') ): ?>
          <div class="row">
            <?php while ( have_rows('featured_reviews') ): the_row(); ?>
              <div class="col xs12<?php if ( get_row_index() !== 3 ): ?> l6<?php endif; ?>">
                <div class="featured-reviews__item">
                  <?php if ( get_sub_field('name') ): ?>
                    <p class="featured-reviews__item-name"><?php the_sub_field('name'); ?></p>
                  <?php endif; ?>

                  <?php if ( get_sub_field('content') ): ?>
                    <div class="featured-reviews__item-content"><?php the_sub_field('content'); ?></div>
                  <?php endif; ?>

                  <?php if ( get_sub_field('stars') ): ?>
                    <ul class="featured-reviews__item-stars">
                      <?php for ( $i = 1; $i <= get_sub_field('stars'); $i++): ?>
                        <li class="featured-reviews__item-star">
                          <span class="visually-hidden"><?php _e('Star', 'iiko'); ?></span>
                        </li>
                      <?php endfor; ?>
                    </ul>
                  <?php endif; ?>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>