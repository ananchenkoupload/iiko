<section
  class="video"
  data-section-type="video"
>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <div
          class="video__video lazyload"
          data-bgset="<?php the_field('video_placeholder'); ?>"
        >
          <button
            class="video__video-play"
            role="button"
            js-video="videoPlay"
          >
          <svg class="icon icon__play" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 93 125">
            <defs>
              <path id="a" d="M382 12h93v125h-93z"/>
            </defs>
            <g transform="translate(-382 -12)" fill="none" fill-rule="evenodd">
              <image x="382" y="12" width="93" height="125" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF0AAAB9CAYAAADA3W/1AAAAAXNSR0IArs4c6QAABgFJREFUeAHtnctvVVUUxu9FSVATJpIwYCDBAQNBxYTHwMR0pmKdOZDigyhQE4bCHPgDrHNphw7KTAgMeDRpE40USHzCBFsTExgAE8ojwZRvUS4s7uP0PNY+Z+291k1Wu99n79/+ctp+99zd1uLi4suIHxD3EAuICcSalr/CEQDgY4ju13UUDIe7qvGRAfduN3GWH0d6tXFE8stngAcl51ExJH9lwyMOIt2nfAxlqwyjklt6H7hZRVdQuVXu6kZHyiI8oO4hyo8gVhpFVn3ZA8DmKb6ERpuqz8DgCHnoZrS5j7qDiBUG0ZVfcgbQIlXTaLyh/CyM9SxCdpm2d1C/H9E2hrD4cpcBWab6FDqtKz4TQz3KUM3R5xba7DKEsdhScwCs0mQSnV8tNiMDrasQzdmXzLOPDKDMvcQ6ftVbi9n8BPDkZrp5BhhtUmvuLarecB5D7Gm32+erDxXvCHUondN5DZmz2GfT5lndSucbcBWZz6D6C7zQQrpupXOmG5H5Gao3Z541qXS+AZeR+Ryq/4MXpppuUumc6RZkZqF6E+aZFqXzDZhB5guo/hovTCmtRemc6bvI/AbVJ2ueaVQ634DTyHwN1f/HC2NPa1Q6Z/o+Mr9D9UmZZ9qVzjfgODKjUP1NXhhjOiboxPcGgm43J2KE3Zmz9ttLZ56d70mYZ7EpvQOfvkdrnsWmdA49WvMsZqXzDYjKPItZ6Rx6VOZZKkrnG6DePEtF6Ry6evMsRaXzDSDzjCzjf3hh0+kUlc6ZknlGNsI+hJonz1JXOt+AU8js1WCepa50Dv0DZMgy/pQXNpG2pHTOdxKZb5oyz6xCpw0g8+wrgD9JmTpflm4v3VzJPDuB2w19cLnWJ88sK51vwhwye6D6KV4YKm1Z6ZzpemTOQfHfIYJ/bNOVztEvpck82w3Vz/ZWyZS40ns5knn2CxR/GBHkY5uu9F7ovOQSMmQj/MkLq6Zd6dkE30H1RSj+W4QYK1d6NnReK2aeie0en12iaTHzzJVeTiGVzDNXejnolcwzV3o56LxXYfPMoXN85dOFzDO/vZQHzXsWMs9c6RydTHoOw2SaZ650GdB8lPXIZJpnrnSOSz7d1zxzpcuD5iP2Nc9c6RxR2PRT88yhhwXdPfo9FAw79G4s4fN/OfTwkHuu4D9Ie5AEL/jblR6c8XMXoHv6x67055gEzdBvL1vx1t8Zhx6U8+PB/8fXI4gdnfdaXwx/TdNX8L9Ia9z+RVxrDPE21N3z/IwrXX4n5jCku4zyXAeOeAw1b0HdUwNboMKVnkUnf52/c5SflUhLeo/0Dag793PurvTy3G+h6wHA/rHoEA69KLGl9v7cSzlupXotoNcoYicUXvqIK1d6fvYzaCpyip7bAMtDf4AmhxDvQd0ixxa60rOhBzncwZXeHzqZVEcR26Fu8aNoXem90MmkCnratSv9GXQyqb5HkEkV9HhxV/oS9FoPYXOlt1rj4P4m1H1+iX/4r5aVTiZVIwdrWlU6HSFLJlUjJ5lag34bsEcA+xNEY2f2Wrq9qDkW3ILSOybVh1B3aZNK8sdr6kqfASwRk0oSeqpKFzepJKGnqPQgJpUk9JSUHtSkkoSeitKDm1SS0GNXem0mlST0mJVeq0klCT1WpdduUklCj03pjZlUktBjUnqjJpU16CpMKkno2m8vakwqSehaby/qTCpJ6BqVPoMFqjOpJKFrUrpqk0oSuhalqzepJKE3rfRoTCpJ6E0qnUwqOsf2V8kFxTBWU0qnJ6m2WAROoqhb6f/iml8Cdm0P9tAitb3qVPoEFr/ZOvDHAsAR1aFf13GBYW1qa3Q+gYlPYvw1jS5Q48UDQb+NcUc0rlfFnAJAP40x16lYnNZJCEK/g7FGEWr+G6JW5i0h6NMY53W1i9Q2sYrQH6D/QcQL2talej4VoF9G302qF6d1ciWgP0Sfo4gg/3hJKyfReRWEfgXtt4lOwOJgBaCPoe1LFhmJrzkH9Hm0GRK/sOUBAfRuBvhx1K22zCfI2gGVwHa/3KQKQvvJoKD9CoLA30csICYQblIFhP4I6g/5uIUjA8QAAAAASUVORK5CYII="/>
              <use fill-opacity="0" fill="#000" xlink:href="#a"/>
            </g>
          </svg>
          </button>

          <?php if ( get_field('video_id') ): ?>
            <div
              class="video__video-embed"
              data-video="<?php the_field('video_id'); ?>"
              js-video="video"
            >
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
