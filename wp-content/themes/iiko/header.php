<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W59V3Q9');</script>
	<!-- End Google Tag Manager -->
    <?php
    /**
     * Load comment reply JS.
     */
    if ( is_singular() ) wp_enqueue_script('comment-reply');

    /**
     * Additional scripts.
     */
    if ( get_field('additional_scripts', 'option') ):
      the_field('additional_scripts', 'option');
    endif;

    wp_head();
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121468690-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-121468690-1');
    </script>
	</head>
  <body <?php body_class(); ?>>
	  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W59V3Q9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <script
      id="hs-script-loader"
      type="text/javascript"
      async
      defer
      src="//js.hs-scripts.com/6316159.js"
    >
    </script>

    <a class="skip-link visually-hidden" href="#MainContent">
      <?php _e('Skip to content', 'iiko'); ?>
    </a>

    <?php get_template_part('template-parts/menu', 'drawer'); ?>

    <?php
    if ( is_page() ){
      $current_template = get_page_template_slug( get_queried_object_id() );
      $templates = wp_get_theme()->get_page_templates();
      $template_handle = sanitize_title_with_dashes( $templates[$current_template] );

      if ( $template_handle === 'success' ):
        $header_modifiers = 'is-fixed';
      endif;
    }
    ?>

    <header role="banner" class="site-header <?php echo $header_modifiers; ?>" js-site-header="container">
      <div class="container">
        <div class="row">
          <div class="col xs12">
            <div class="site-header__main">
              <div class="site-header__logo">
                <?php the_custom_logo(); ?>

                <?php if ( get_field('header_alternative_logo', 'option') ): ?>
                  <a href="<?php echo get_site_url(); ?>">
                    <img
                      class="custom-logo custom-logo--alternative"
                      alt="<?php echo get_bloginfo('name'); ?>"
                      src="<?php the_field('header_alternative_logo', 'option'); ?>"
                    >
                  </a>
                <?php endif; ?>
              </div>

              <nav class="site-header__nav" js-site-header="siteNav" role="navigation">
                <?php get_template_part('template-parts/site', 'nav'); ?>
              </nav>

              <div class="site-header__misc">
                <?php if ( get_field('header_call_to_action', 'option') ): ?>
                  <a
                    class="site-header__cta button button--outlined"
                    href="<?php the_field('header_call_to_action_link', 'option'); ?>"
                  >
                    <?php the_field('header_call_to_action', 'option'); ?>
                  </a>
                <?php endif; ?>
              </div>

              <div class="site-header__toggle">
                <button class="site-header__toggle-button" js-toggle="MenuDrawer">
                <svg viewBox="0 0 36 36" class="icon icon__menu" xmlns="http://www.w3.org/2000/svg">
                  <g stroke="#000" stroke-width="1.8" fill="none" fill-rule="evenodd" stroke-linecap="square">
                    <path d="M6.88 10H29.1M7 18.5h22.22M7 27h22.22"/>
                  </g>
                </svg>

                  <span class="visually-hidden">
                    <?php _e('Open menu drawer', 'iiko'); ?>
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <main id="MainContent" class="site-main" role="main">
